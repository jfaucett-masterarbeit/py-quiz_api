## Architecture

There are 2 distinct portions of the application

1. The API interface (quiz_api): contains views, handlers, etc. It parses the URL, validates parameters, loads translation files and performs all the necessary setup and validation. If everything checks out it sends the parameters on to the module for handling the given question type.
2. The Module interface (quiz_libs): contains all the code for actually automatically grading question items. There are handlers in `quiz_libs/handlers.py` which take in parameters from the api interface, call the grading module, and return a well-formatted response to the caller. This response is then set back to the application requesting service from the quiz-API itself. Each module for grading a particular question type can be found in the `quiz_libs/questions_module/` directory. At the time of this writing there are only 2 question types `short_answer` and `fill_in_the_blank`.

#### Endpoints

1. `GET http://localhost:4010/api` : the index of the API shows a list of available urls with descriptive information.
2. `POST http://localhost:4010/api/question_types/short_answer` : endpoint for automatically grading short answer question items.
3. `GET http://localhost:4010/api/question_types/short_answer` : endpoint for getting information about what data needs to be sent to grade short answer question items.
4. `POST http://localhost:4010/api/question_types/fill_in_the_blank` : endpoint for automatically grading fill-in-the-blank question items.
5. `GET http://localhost:4010/api/question_types/fill_in_the_blank` : endpoint for getting information about what data needs to be sent to grade fill-in-the-blank question items.
