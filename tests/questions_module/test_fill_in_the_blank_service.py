import pytest

from quiz_libs.utils import load_translations
import quiz_libs.service as service

load_translations('en')


def create_data(params={}):
    data = {
        'question_type': 'fill_in_the_blank',
        'domain': 'computer_science',
        'language': 'en',
        'pass_fail': False,
    }
    data.update(params)
    return data


class TestFillInTheBlankService:

    def test_dispatching_correct_question(self):

        data = create_data({
            'slots': ['Germany'],
            'slot_answers': [['Germany', 'France', 'Austria', 'Italy']],
            'options': {},
            'points_available': 10,
            'pass_fail_threshold': 0.5
        })

        response = service.dispatch_question(data)
        assert response['feedback'][0] in [
            "correct!", "exact match!", "perfect!", "That's right.", "Good job!"]
        assert response['score']['raw'] == 1.0
        assert response['score']['scaled'] == 10
        assert response['log'] == []

    def test_dispatching_incorrect_question(self):

        data = create_data({
            'slots': ['Japan'],
            'slot_answers': [['Germany', 'France', 'Austria', 'Italy']],
            'options': {},
            'points_available': 10,
            'pass_fail_threshold': 0.5
        })

        response = service.dispatch_question(data)
        assert response['feedback'][0] in [
            "Sorry, that's incorrect.", "That's the wrong answer.", "Nope, sorry."]
        assert response['score']['raw'] == 0.0
        assert response['score']['scaled'] == 0
        assert response['log'] == []

    def test_options(self):

        data = create_data({
            'slots': ['japn'],
            'slot_answers': [['Japan', 'France', 'Austria', 'Italy']],
            'options': {
                'normalize_case': True,
                'edit_distance': 1
            },
            'points_available': 100,
            'pass_fail_threshold': 0.5
        })

        response = service.dispatch_question(data)
        assert response['score']['raw'] == 1.0
        assert response['score']['scaled'] == 100
        assert response['log'] == []
