import pytest

from quiz_libs.questions_module.fill_in_the_blank import SlotAnalyzer, CollectionAnalyzer, FeedbackGenerator, Scorer


class TestSlotAnalyzer:

    def test_fitb_correct(self):
        opts = {}
        correct_answers = ['Germany', 'Denmark', 'Sweden', 'England', 'Norway']
        q = SlotAnalyzer('Denmark', correct_answers, opts)
        result = q.analyze()

        assert len(result) == 5
        assert result[0] == 'match'
        assert result[1] == ['Denmark', 'Denmark', 'Denmark']
        assert result[2] == []
        assert result[3] == 1
        assert result[4] == correct_answers

    def test_fitb_incorrect(self):
        opts = {}
        correct_answers = ['Germany', 'Denmark', 'Sweden', 'England', 'Norway']
        q = SlotAnalyzer('USA', correct_answers, opts)
        result = q.analyze()

        assert len(result) == 5
        assert result[0] == 'no_match'
        assert result[1] == []
        assert result[2] == []
        assert result[3] == -1
        assert result[4] == correct_answers, "When there is not match the result stores all the correct answers"

    def test_fitb_correct_w_whitespace_normalization(self):
        opts = {}
        correct_answers = ['Germany', 'Denmark', 'Sweden', 'England', 'Norway']
        q = SlotAnalyzer('    Germany   ', correct_answers, opts)
        result = q.analyze()

        assert len(result) == 5
        assert result[0] == 'match'
        assert result[1] == ['    Germany   ', 'Germany', 'Germany']
        assert result[2] == []
        assert result[3] == 0
        assert result[4] == correct_answers

    def test_fitb_correct_w_whitespace_normalization_on_multi_words(self):
        opts = {}
        correct_answers = ['United States',
                           'The United States', 'United States of America']
        q = SlotAnalyzer(
            '  United States       of      America   ', correct_answers, opts)
        result = q.analyze()

        assert len(result) == 5
        assert result[0] == 'match'
        assert result[1] == [
            '  United States       of      America   ', 'United States of America', 'United States of America']
        assert result[2] == []
        assert result[3] == 2
        assert result[4] == correct_answers

    def test_fitb_correct_w_ignore_case(self):
        opts = {'normalize_case': True}
        correct_answers = ['Germany', 'Denmark', 'Sweden', 'England', 'Norway']
        q = SlotAnalyzer('sweden', correct_answers, opts)
        result = q.analyze()

        assert len(result) == 5
        assert result[0] == 'corrected_match'
        assert result[1] == ['sweden', 'sweden', 'Sweden']
        assert result[2] == ['normalize_case']
        assert result[3] == 2
        assert result[4] == correct_answers

    def test_fitb_correct_w_ignore_case_and_whitespace_normalization_1(self):
        opts = {'normalize_case': True}
        correct_answers = ['Germany', 'Denmark', 'Sweden', 'England', 'Norway']
        q = SlotAnalyzer('    ENGland', correct_answers, opts)
        result = q.analyze()

        assert len(result) == 5
        assert result[0] == 'corrected_match'
        assert result[1] == ['    ENGland', 'england', 'England']
        assert result[2] == ['normalize_case']
        assert result[3] == 3
        assert result[4] == correct_answers

    def test_fitb_correct_w_ignore_case_and_whitespace_normalization_2(self):
        opts = {'normalize_case': True, 'normalize_whitespace': True}
        correct_answers = ['Germany', 'Denmark', 'Sweden', 'England', 'Norway']
        q = SlotAnalyzer('ENGLAND', correct_answers, opts)
        result = q.analyze()

        assert len(result) == 5
        assert result[0] == 'corrected_match'
        assert result[1] == ['ENGLAND', 'england', 'England']
        assert 'normalize_case' in result[2] and not 'normalize_whitespace' in result[2]
        assert result[3] == 3

    def test_fitb_correct_w_ignore_case_and_edit_distance(self):
        opts = {'normalize_case': True, 'edit_distance': 1}
        correct_answers = ['Germany', 'Denmark', 'Sweden', 'England', 'Norway']
        q = SlotAnalyzer('INGLAND', correct_answers, opts)
        result = q.analyze()

        assert len(result) == 5
        assert result[0] == 'corrected_match'
        assert result[1] == ['INGLAND', 'ingland', 'England']
        assert 'normalize_case' in result[2] and 'edit_distance' in result[2]
        assert result[3] == 3


class TestCollectionAnalyzer:

    def test_fitbc_correct(self):

        slot_set = ['Shakespeare', 'Hamlet']
        slot_set_answers = [['Shakespeare'], [
            'King Lear', 'The Tempest', 'Hamlet']]

        coll = CollectionAnalyzer(slot_set, slot_set_answers, {})
        results = coll.analyze()

        idx = 0
        for result in results:
            assert len(result) == 5
            assert result[0] == 'match'

            if idx == 0:
                assert result[1] == ['Shakespeare',
                                     'Shakespeare', 'Shakespeare']
            else:
                assert result[1] == ['Hamlet', 'Hamlet', 'Hamlet']
            idx += 1

    def test_fitbc_incorrect(self):

        slot_set = ['Shakespeare', 'Faust']
        slot_set_answers = [['Shakespeare'], [
            'King Lear', 'The Tempest', 'Hamlet']]

        coll = CollectionAnalyzer(slot_set, slot_set_answers, {})
        results = coll.analyze()

        assert len(results) == 2
        assert results[0][0] == 'match'
        assert results[1][0] == 'no_match'


class TestFillInTheBlankScorer:

    def test_fitb_scorer(self):
        slot_set = ['Shakespeare', 'Faust']
        slot_set_answers = [['Shakespeare'], [
            'King Lear', 'The Tempest', 'Hamlet']]

        coll = CollectionAnalyzer(slot_set, slot_set_answers, {})
        results = coll.analyze()
        scorer = Scorer()

        score = scorer.score(results)
        assert score == 0.5

        slot_set2 = ['Shakespeare', 'The Tempest']
        coll2 = CollectionAnalyzer(slot_set2, slot_set_answers, {})
        results2 = coll2.analyze()
        scorer2 = Scorer()

        assert scorer2.score(results2) == 1.0
