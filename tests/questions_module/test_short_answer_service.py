import pytest
import numpy as np

import quiz_libs.service as service


def create_data(params={}):
    data = {
        'question_type': 'short_answer',
        'language': 'en',
        'pass_fail': False,
    }
    data.update(params)
    return data


class TestShortAnswer_WordOverlap_CosineCoefficient:
    """
    The cosine coefficient gives students a score based on how many words in their response
    are shared by words in the model answer. Word counts are ignored.
    """

    def test_correct_response(self):

        data = create_data({
            'model_answer': 'a function has a name, arguments and a return type',
            'student_answer': 'function has a name, arguments, and return type',
            'points_available': 10,
            'method': 'cosine_coefficient',
            'debug': False
        })

        response = service.dispatch_question(data)

        assert response['feedback'] == []
        assert abs(response['score']['raw'] - 1.0) < 0.0001
        assert response['score']['scaled'] == 10
        assert response['log'] == []

    def test_wrong_response(self):

        data = create_data({
            'model_answer': 'a function has a name, arguments and a return type',
            'student_answer': 'it might have a body which transforms data',
            'points_available': 10,
            'method': 'cosine_coefficient',
            'debug': False
        })

        response = service.dispatch_question(data)

        assert response['feedback'] == []
        assert response['score']['raw'] == 0
        assert response['score']['scaled'] == 0.0
        assert response['log'] == []

    def test_partially_correct_response(self):

        data = create_data({
            'model_answer': 'a function has a name, arguments and a return type',
            'student_answer': 'a function has a name and a body',
            'points_available': 10,
            'method': 'cosine_coefficient',
            'debug': False
        })

        response = service.dispatch_question(data)

        assert response['feedback'] == []
        assert np.logical_and(
            response['score']['raw'] > 0.5, response['score']['raw'] < 1.0)
        assert response['score']['scaled'] == 5.0
        assert response['log'] == []


class TestShortAnswer_WordOverlap_FaucettCoefficient:
    """
    The faucett coefficient is more lenient than the cosine coefficient.
    It gives students a higher score for less word overlap.
    """

    def test_correct_response(self):

        data = create_data({
            'model_answer': 'a function has a name, arguments and a return type',
            'student_answer': 'function has a name, arguments, and return type',
            'points_available': 10,
            'method': 'faucett_coefficient',
            'method_options': {},
            'debug': False
        })

        response = service.dispatch_question(data)

        assert response['feedback'] == []
        assert abs(response['score']['raw'] - 1.0) < 0.0001
        assert response['score']['scaled'] == 10
        assert response['log'] == []

    def test_wrong_response(self):

        data = create_data({
            'model_answer': 'a function has a name, arguments and a return type',
            'student_answer': 'it might have a body which transforms data',
            'points_available': 10,
            'method': 'faucett_coefficient',
            'method_options': {},
            'debug': False
        })

        response = service.dispatch_question(data)

        assert response['feedback'] == []
        assert response['score']['raw'] == 0
        assert response['score']['scaled'] == 0.0
        assert response['log'] == []

    def test_partially_correct_response(self):

        data = create_data({
            'model_answer': 'a function has a name, arguments and a return type',
            'student_answer': 'a function has a name and a body',
            'points_available': 10,
            'method': 'faucett_coefficient',
            'method_options': {},
            'debug': False
        })

        response = service.dispatch_question(data)

        assert response['feedback'] == []
        assert np.logical_and(
            response['score']['raw'] > 0.5, response['score']['raw'] < 1.0)
        assert response['score']['scaled'] == 6.0
        assert response['log'] == []
