from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from quiz_api.views import IndexView, ShortAnswerView, FillInTheBlankView

urlpatterns = [
    url(r'^api/question_types/short_answer$',
        ShortAnswerView.as_view(), name='short_answer_index'),
    url(r'^api/question_types/fill_in_the_blank$',
        FillInTheBlankView.as_view(), name='fill_in_the_blank_index'),
    url(r'^api', IndexView.as_view(), name='api_main_index')
]
