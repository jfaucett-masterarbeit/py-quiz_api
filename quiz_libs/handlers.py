from io import StringIO
import logging
import quiz_libs.questions_module.fill_in_the_blank as fitb
import quiz_libs.questions_module.short_answer as asag
from quiz_libs.scoring import ScaleAndRoundScorer
import i18n


def create_handler(settings):
    question_type = settings['question_type']
    if question_type == 'fill_in_the_blank':
        return FillInTheBlankHandler(settings)
    elif question_type == 'short_answer':
        return ShortAnswerHandler(settings)
    else:
        raise Exception(
            '[unknown question_type]: {}'.format(question_type))


class AbstractHandler(object):

    def __init__(self, settings):
        self.settings = settings
        self.log_buffer = False
        self.log_handler = None
        self.setup_logging()

    def in_debug_modus(self):
        return 'debug' in self.settings and self.settings['debug']

    def log_info(self, message):
        if self.log_buffer:
            self.log.info(message)

    def log_error(self, message):
        if self.log_buffer:
            logging.error(message)

    def setup_logging(self):
        if self.in_debug_modus():
            self.log_buffer = StringIO()
            self.log_handler = logging.StreamHandler(self.log_buffer)
            self.log = logging.getLogger(self.__class__.__name__)
            self.log.setLevel(logging.INFO)
            for handler in self.log.handlers:
                self.log.removeHandler(handler)
            self.log.addHandler(self.log_handler)

    def get_log(self):
        if self.in_debug_modus():
            self.log_handler.flush()
            return self.log_buffer.getvalue()

    def cleanup(self):
        if self.in_debug_modus():
            self.log_handler.close()

    def call(self):
        pass


class FillInTheBlankHandler(AbstractHandler):

    def call(self):
        # set the correct language
        i18n.set('locale', self.settings['language'])

        # create the analyzer
        collection_analyzer = fitb.CollectionAnalyzer(
            slots=self.settings['slots'],
            slot_answers=self.settings['slot_answers'],
            options=self.settings['options'])

        # create the scorer
        scorer = fitb.Scorer(
            pass_fail=self.settings['pass_fail'],
            pass_fail_threshold=self.settings['pass_fail_threshold']
        )

        # analyze the questions
        results = collection_analyzer.analyze()

        # score the questions
        raw_score = scorer.score(results)
        score_normalizer = ScaleAndRoundScorer(
            self.settings['points_available'])
        scaled_score = score_normalizer.scale(raw_score)

        # gather feedback about the questions
        fg = fitb.FeedbackGenerator(results)
        feedback = fg.generate_feedback()

        # build the response
        response = {
            'score': {
                'raw': raw_score,
                'scaled': scaled_score
            },
            'feedback': feedback,
            'log': []
        }

        # append the log if requested
        if self.in_debug_modus():
            response['log'] = self.get_log()

        return response


class ShortAnswerHandler(AbstractHandler):

    def call(self):
        # set the correct language
        i18n.set('locale', self.settings['language'])

        # create the scorer
        scorer = asag.create_scorer(self)

        # score the questions
        raw_score = scorer.score()

        score_normalizer = ScaleAndRoundScorer(
            self.settings['points_available'])
        scaled_score = score_normalizer.scale(raw_score)

        # build the response
        response = {
            'score': {
                'raw': raw_score,
                'scaled': scaled_score
            },
            'feedback': [],
            'log': []
        }

        # append the log if requested
        if self.in_debug_modus():
            response['log'] = self.get_log()

        return response
