import numpy as np


class ScaleAndRoundScorer(object):

    def __init__(self, points_available):
        self.points_available = points_available

    def scale(self, raw_score):
        return np.round(raw_score * self.points_available)
