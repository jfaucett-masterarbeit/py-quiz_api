from quiz_libs.handlers import create_handler


def dispatch_question(data):
    handler = create_handler(data)
    return handler.call()
