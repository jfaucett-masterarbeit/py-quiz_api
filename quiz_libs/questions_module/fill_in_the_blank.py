from quiz_libs.preprocessing.strings import normalize_whitespace, lowercase
import numpy as np
import i18n
from nltk import edit_distance


class CollectionAnalyzer(object):

    def __init__(self, slots, slot_answers, options):
        self.slots = slots
        self.slot_answers = slot_answers
        self.options = options

    def analyze(self):
        results = []
        index = 0
        n = len(self.slots)

        while index < n:

            slot = self.slots[index]
            slot_answers = self.slot_answers[index]
            slot_analyzer = SlotAnalyzer(slot, slot_answers, self.options)
            result = slot_analyzer.analyze()
            results.append(result)

            index += 1

        return results


class SlotAnalyzer(object):

    def __init__(self, blank_slot, slot_answers, options={}):
        self.blank_slot = blank_slot
        self.slot_answers = slot_answers
        self.options = options

    def analyze(self):
        """
        Returns a 4-Element list containing the following

        0 : a keyword denoting the outcome, can be one of: match|corrected_match|no_match
        1 : A 3-tuple containing the original unprocessed input, the processed input, and the matching item
        2 : a list of strings which indicate which options where applied to get the match.
            It can be one of: normalize_case|edit_distance
        3 : the (zero-based) index of the correct answer which matched. Is -1 for no_match.
        4 : A List of all the correct answers
        """
        slot = normalize_whitespace(self.blank_slot)
        slot_lc = lowercase(slot)
        result = []

        # options
        ignore_case = self.options.get('normalize_case', False)
        edit_distance_option = self.options.get('edit_distance', 0)

        index = 0
        for slot_answer in self.slot_answers:
            slot_answer = normalize_whitespace(slot_answer)
            slot_answer_lc = lowercase(slot_answer)

            if slot_answer == slot:
                result = ['match', [self.blank_slot, slot, slot_answer],
                          [], index, self.slot_answers.copy()]
                break
            elif ignore_case and edit_distance_option:
                if slot_lc == slot_answer_lc:
                    result = ['corrected_match', [self.blank_slot, slot_lc, slot_answer], [
                        'normalize_case'], index, self.slot_answers.copy()]
                    break
                if edit_distance(slot, slot_answer) <= edit_distance_option:
                    result = ['corrected_match', [self.blank_slot, slot_answer, slot_answer], [
                        'edit_distance'], index, self.slot_answers.copy()]
                    break
                if edit_distance(slot_lc, slot_answer_lc) <= edit_distance_option:
                    result = ['corrected_match', [self.blank_slot, slot_lc, slot_answer],
                              ['normalize_case', 'edit_distance'], index, self.slot_answers.copy()]
                    break
            elif ignore_case:
                if slot_lc == slot_answer_lc:
                    result = ['corrected_match', [self.blank_slot, slot_lc, slot_answer], [
                        'normalize_case'], index, self.slot_answers.copy()]
                    break
            elif edit_distance_option:
                if edit_distance(slot, slot_answer) <= edit_distance_option:
                    result = ['corrected_match', [self.blank_slot, slot_answer, slot_answer], [
                        'edit_distance'], index, self.slot_answers.copy()]
                    break
            index += 1

        if len(result) == 0:
            result = ['no_match', [], [], -1, self.slot_answers.copy()]

        return result


class MatchFeedback(object):

    OPTIONS = 5

    def __init__(self, result):
        self.result = result

    def generate(self):
        phrase_index = np.random.choice(self.OPTIONS) + 1
        phrase = 'fill_in_the_blank.match.option_{}'.format(phrase_index)
        return i18n.t(phrase)


class CorrectedMatchFeedback(object):

    OPTIONS = 2

    def __init__(self, result):
        self.result = result

    def generate(self):
        phrase_index = np.random.choice(self.OPTIONS) + 1
        phrase = 'fill_in_the_blank.corrected_match.option_{}'.format(
            phrase_index)

        # possible answers
        and_joiner = i18n.t('fill_in_the_blank.and_joiner')
        values = list(map(lambda x: i18n.t(
            'fill_in_the_blank.corrections.{}'.format(x)), self.result[2]))
        corrections = and_joiner.join(values)

        possible_answers_length = len(self.result[4])
        or_joiner = i18n.t('fill_in_the_blank.or_joiner')
        answers = or_joiner.join(
            list(map(lambda x: "`{}`".format(x), self.result[4])))

        return i18n.t(phrase,
                      count=possible_answers_length,
                      corrections=corrections,
                      user_input=self.result[1][0],
                      model_input=answers)


class NoMatchFeedback(object):

    OPTIONS = 3

    def __init__(self, result):
        self.result = result

    def generate(self):
        phrase_index = np.random.choice(self.OPTIONS) + 1
        phrase = 'fill_in_the_blank.no_match.option_{}'.format(
            phrase_index)
        return i18n.t(phrase)


class FeedbackGenerator(object):

    def __init__(self, results):
        self.results = results

    def generate_feedback(self):
        index = 0
        notes = []

        while index < len(self.results):
            result = self.results[index]
            note = ''

            if result[0] == 'match':
                note = MatchFeedback(result).generate()
            elif result[0] == 'corrected_match':
                note = CorrectedMatchFeedback(result).generate()
            elif result[0] == 'no_match':
                note = NoMatchFeedback(result).generate()

            notes.append(note)

            index += 1

        return notes


class Scorer(object):

    def __init__(self, pass_fail=False, pass_fail_threshold=0.0):
        self.pass_fail = pass_fail
        self.pass_fail_threshold = pass_fail_threshold

    def score(self, results=[]):
        n = len(results)
        score_sum = 0

        for result in results:
            if result[0] == 'match' or result[0] == 'corrected_match':
                score_sum += 1

        fp_score = score_sum / n

        if self.pass_fail:
            if fp_score >= self.pass_fail_threshold:
                return 1.0
            else:
                return 0.0
        else:
            return fp_score
