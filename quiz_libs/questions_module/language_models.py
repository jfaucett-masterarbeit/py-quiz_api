import os
from gensim.models import KeyedVectors

LOADED_MODELS = {}


def load_language_model(key):
    root_dir = os.path.dirname(os.path.dirname(
        os.path.dirname(os.path.abspath(__file__))))
    vsm_dir = os.path.join(root_dir, "vector_space_models")
    vsm_file = os.path.join(vsm_dir, key)
    if not key in LOADED_MODELS:
        language_model = KeyedVectors.load(vsm_file)
        LOADED_MODELS[key] = language_model
    return LOADED_MODELS[key]
