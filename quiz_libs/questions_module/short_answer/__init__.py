from . import word_overlap
from . import ml_model_v1
import numpy as np

AVAILABLE_ASAG_MODELS = [

    # Word Overlap Methods
    'cosine_coefficient', 'faucett_coefficient',

    # Machine Learning Methods
    'ml_model_v1.0'
]


def create_scorer(handler):

    asag_method = handler.settings['method']

    if asag_method == 'cosine_coefficient':
        return word_overlap.CosineCoefficientModel(handler)
    elif asag_method == 'faucett_coefficient':
        return word_overlap.FaucettCoefficientModel(handler)
    elif asag_method == 'ml_model_v1.0':
        return ml_model_v1.MLModelVersion1(handler)
    else:
        raise Exception("unrecognized ASAG method: {}".format(asag_method))
