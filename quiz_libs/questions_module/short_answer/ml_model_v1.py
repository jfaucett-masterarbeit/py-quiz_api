import numpy as np
import os
import pickle
import nltk
from scipy import spatial
from zss import simple_distance, Node
from sklearn.feature_extraction.text import CountVectorizer
import spacy

import quiz_libs.questions_module.language_models as language_models
import quiz_libs.questions_module.vsm as vsm
import quiz_libs.preprocessing.tokens as pp_tokens

from sklearn.neural_network import MLPClassifier


# ===============================
# LEXICAL STRUCTURE FUNCTIONS
# ===============================


def cosine_coefficient(s1, s2):
    s1 = set(s1)
    s2 = set(s2)
    num = len(s1.intersection(s2))
    den = np.sqrt(len(s1)) * np.sqrt(len(s2))
    return num / (den + 1e-30)


def create_count_vectorizer_fn(tokenizer_fn, ngram_range, stopwords):

    count_vec = CountVectorizer(
        tokenizer=tokenizer_fn, ngram_range=ngram_range, stop_words=stopwords)

    def count_vectorizer_fn(ma, sa):
        try:
            x = count_vec.fit_transform([ma, sa])
        except ValueError:
            return 0.0

        mat = x.toarray()
        ma_vec = np.array(mat[0])
        sa_vec = np.array(mat[1])
        value = spatial.distance.cosine(ma_vec, sa_vec)
        if np.isnan(value):
            return 0.0
        else:
            return 1 - spatial.distance.cosine(ma_vec, sa_vec)

    return count_vectorizer_fn


def create_ngram_scorer(tokenizer_fn, ngrams, stopwords, weights=[]):

    if len(weights) != ngrams:
        raise Exception(
            'weights must be an array of values equal in length to the number of ngrams')

    scorers = []
    for k in range(1, ngrams+1):
        count_vec = create_count_vectorizer_fn(
            tokenizer_fn, ngram_range=(k, k), stopwords=stopwords)
        scorers.append(count_vec)

    def ngram_scorer(ma, sa):
        scores = []
        for score_fn in scorers:
            score = score_fn(ma, sa)
            scores.append(score)

        scores = np.array(scores)
        return np.min([1.0, scores.dot(weights)])

    return ngram_scorer

# ===============================
# VECTOR SPACE MODEL FUNCTIONS
# ===============================


MAX_WMD_VALUE = 4.5


def create_wmsimilarity(model):

    def word_mover_similarity(s1, s2):
        """
        Uses gensims built in Word Mover Distance measurement.
        """
        value = model.wmdistance(s1, s2)
        if np.isinf(value) or np.isnan(value):
            return 0.0
        else:
            return max(1.0 - (value/MAX_WMD_VALUE), 0.0)

    return word_mover_similarity


def spacy_cosine_similarity(ma, sa):
    return ma.similarity(sa)


def create_word_to_word_similarity_function(model):

    def word_to_word_vector_similarity(v1, v2):
        n = len(v1)
        if n == 0:
            return 0.0

        sims = 0
        for i in range(len(v1)):
            x1 = v1[i]
            x2 = v2[i]

            try:
                sims += model.similarity(x1, x2)
            except KeyError:
                # word x1 or x2 is not in vocabulary
                pass

        return sims / n

    return word_to_word_vector_similarity

# ===============================
# SYNTAX FUNCTIONS
# ===============================


def get_node_label(node):
    if node.pos_ == 'PRON':
        return node.text.lower()
    else:
        # check for negation
        if 'neg' in [t.dep_ for t in node.children]:
            return 'neg_{}'.format(node.lemma_)
        else:
            return node.lemma_


def to_tree(node):
    if node.n_lefts + node.n_rights > 0:
        label = get_node_label(node)
        zss_Node = Node(label)
        for child in node.children:
            zss_Node.addkid(to_tree(child))
        return zss_Node
    else:
        label = get_node_label(node)
        zss_Node = Node(label)
        return zss_Node


def get_root(sent):
    return [t for t in sent if t.dep_ == 'ROOT'][0]


def tree_distance(s1, s2):
    tree1 = to_tree(get_root(s1))
    tree2 = to_tree(get_root(s2))
    return simple_distance(tree1, tree2)


def create_token_edit_distance(ignore=['DET', 'PUNCT'], stopwords=[]):

    def feature_token_edit_distance(ma, sa):
        ma1 = [get_node_label(t) for t in ma if len(
            t.dep_) > 0 and not t.pos_ in ignore]
        ma1 = [t for t in ma1 if not t in stopwords]
        sa1 = [get_node_label(t) for t in sa if len(
            t.dep_) > 0 and not t.pos_ in ignore]
        sa1 = [t for t in sa1 if not t in stopwords]
        return edit_distance(sa1, ma1)

    return feature_token_edit_distance


def edit_distance(s1, s2):
    return nltk.edit_distance(s1, s2)


def edit_similarity(s1, s2):
    return (1 / (1 + edit_distance(s1, s2)))


def length_diff(ma, sa):
    return (len(ma) - len(sa)) / len(ma)

# ============================================
# Preprocessing Helpers and Data
# ============================================


def load_ml_model(model_name):
    root_dir = os.path.dirname(os.path.dirname(os.path.dirname(
        os.path.dirname(os.path.abspath(__file__)))))
    models_dir = os.path.join(root_dir, 'ml_models')
    file_path = os.path.join(models_dir, model_name)
    return pickle.load(open(file_path, 'rb'))


preprocess_basic = pp_tokens.create_preprocess_basic(pp_tokens.nlp_en)
preprocess_not = pp_tokens.create_preprocess_not(pp_tokens.nlp_en)


ml_model_v1_0 = load_ml_model('ml_model_v1.0.model')


def preprocess_wms(x):
    x2 = preprocess_basic_stops(x)
    x3 = ' '.join(x2)
    return x3


model_en = language_models.load_language_model('fasttext')

# ===============================
# Features
# ===============================
word2word_sim_fn = create_word_to_word_similarity_function(model_en)
token_edit_distance_fn = create_token_edit_distance(
    stopwords=pp_tokens.EN_STOPS)

count_vec_ng1 = create_count_vectorizer_fn(
    preprocess_basic, (1, 1), stopwords=pp_tokens.EN_STOPS)
count_vec_ng2 = create_count_vectorizer_fn(
    preprocess_basic, (2, 2), stopwords=pp_tokens.EN_STOPS)
count_vec_ng3 = create_count_vectorizer_fn(
    preprocess_basic, (3, 3), stopwords=pp_tokens.EN_STOPS)

entailment_sim_ng3 = vsm.create_compute_entailment_similarity(
    word2word_sim_fn, ngram_size=3)

word_mover_sim_fn = create_wmsimilarity(model_en)


def preprocess_basic_stops(x):
    tokens = preprocess_basic(x)
    return [t for t in tokens if not t in pp_tokens.EN_STOPS]


def preprocess_not_stops(x):
    tokens = preprocess_not(x)
    return [t for t in tokens if not t in pp_tokens.EN_STOPS]


class BasicMLModel(object):

    def __init__(self, handler):
        self.handler = handler
        self.params = self.handler.settings

    def in_debug_modus(self):
        return self.handler.in_debug_modus()


class MLModelVersion1(BasicMLModel):

    LENGTH_DIFF_MAX = 1.0
    LENGTH_DIFF_MIN = -23.0

    def score(self):
        ma = self.params['model_answer']
        sa = self.params['student_answer']

        # Preprocess
        ma_basic = preprocess_basic_stops(ma)
        sa_basic = preprocess_basic_stops(sa)
        ma_not = preprocess_not_stops(ma)
        sa_not = preprocess_not_stops(sa)

        print(ma_basic, ma_not)
        print(sa_basic, sa_not)

        # Cosine Coefficients
        feature_cosine_coeff = cosine_coefficient(ma_basic, sa_basic)
        feature_cosine_coeff_not = cosine_coefficient(ma_not, sa_not)

        # Entailment Similarity
        feature_entailment_sim = entailment_sim_ng3(ma_basic, sa_basic)

        # Count Vectors N-Grams (1-3)
        feature_cv1 = count_vec_ng1(ma, sa)
        feature_cv2 = count_vec_ng2(ma, sa)
        feature_cv3 = count_vec_ng3(ma, sa)

        # Length Difference
        feature_length_diff = length_diff(ma_basic, sa_basic)

        # Normalize Length Diff
        feature_length_diff_norm = (
            feature_length_diff - self.LENGTH_DIFF_MIN) / (self.LENGTH_DIFF_MAX - self.LENGTH_DIFF_MIN)

        # Edit Similarity
        feature_token_edit_sim = edit_similarity(ma_basic, sa_basic)

        # Word Mover Similarity
        feature_word_mover_sim = word_mover_sim_fn(
            preprocess_wms(ma), preprocess_wms(sa))

        # TODO: normalize feature_length_diff

        values = np.array([
            feature_cosine_coeff, feature_cosine_coeff_not,
            feature_entailment_sim, feature_word_mover_sim,
            feature_cv1, feature_cv2, feature_cv3,
            feature_length_diff_norm, feature_token_edit_sim
        ]).reshape(1, 9)

        # can have three values [0,1,2]
        result = ml_model_v1_0.predict(values)
        return (result / 2.0)
