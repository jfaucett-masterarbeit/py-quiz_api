import quiz_libs.preprocessing.strings as pp_strings
import quiz_libs.preprocessing.tokens as pp_tokens
import numpy as np

# ======================================
# Word Overlap Methods
# ======================================


wo_pipeline_stem = pp_tokens.create_pipeline({
    'remove_punctuation': pp_tokens.remove_punctuation,
    'remove_stopwords': pp_tokens.remove_stopwords,
    'stem': pp_tokens.stem
}, pp_tokens.whitespace_tokenize)


def wo_preprocess(response):

    # merge all whitespace, convert to lowercase
    response = pp_strings.lowercase(pp_strings.normalize_whitespace(response))

    return wo_pipeline_stem(response)


class BasicModel(object):

    def __init__(self, handler):
        self.handler = handler
        self.params = self.handler.settings

    def in_debug_modus(self):
        return self.handler.in_debug_modus()


class CosineCoefficientModel(BasicModel):

    def score(self):
        ma = self.params['model_answer']
        sa = self.params['student_answer']

        ma_t = wo_preprocess(ma)
        ma_ts = set(ma_t)

        sa_t = wo_preprocess(sa)
        sa_ts = set(sa_t)

        if self.in_debug_modus():
            self.handler.log_info('[cosine_coefficient method]')
            self.handler.log_info(
                "[model answer preprocessed]: {}".format(ma_t))
            self.handler.log_info(
                "[model answer to set]: {}".format(ma_ts))
            self.handler.log_info(
                "[student answer preprocessed]: {}".format(sa_t))
            self.handler.log_info(
                "[student answer to set]: {}".format(sa_ts))

        # calculate the cosine coefficient
        num = len(ma_ts.intersection(sa_ts))
        den = np.sqrt(len(ma_ts)) * np.sqrt(len(sa_ts))
        return (num / den)


class FaucettCoefficientModel(BasicModel):

    DEFAULT_FACTOR = 2

    def score(self):
        ma = self.params['model_answer']
        sa = self.params['student_answer']
        options = self.params['method_options']

        factor = self.get_factor(options)

        ma_t = wo_preprocess(ma)
        ma_ts = set(ma_t)

        sa_t = wo_preprocess(sa)
        sa_ts = set(sa_t)

        if self.in_debug_modus():
            self.handler.log_info(
                '[faucett_coefficient method]: factor={}'.format(factor))
            self.handler.log_info(
                "[model answer preprocessed]: {}".format(ma_t))
            self.handler.log_info(
                "[model answer to set]: {}".format(ma_ts))
            self.handler.log_info(
                "[student answer preprocessed]: {}".format(sa_t))
            self.handler.log_info(
                "[student answer to set]: {}".format(sa_ts))

        # calculate the cosine coefficient
        set_intersection = len(ma_ts.intersection(sa_ts))
        set_union = len(ma_ts.union(sa_ts))

        return np.power(set_intersection / set_union, 1/factor)

    def get_factor(self, options):
        if not 'factor' in options:
            return self.DEFAULT_FACTOR
        else:
            factor = int(options['factor'])
            if factor <= 1:
                raise Exception('[factor must be > 1]: {}'.format(factor))
            return factor
