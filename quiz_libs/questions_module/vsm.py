from gensim.models import KeyedVectors
from gensim.models import Word2Vec
from gensim.models.wrappers import FastText
import spacy
import numpy as np
from nltk import ngrams

# ===============================
# LOADING MODELS
# ===============================


def load_gensim_model(model_path):
    model = KeyedVectors.load_word2vec_format(model_path)
    return model


def load_word2vec(model_path):
    model = Word2Vec.load(model_path)
    return model


def load_fasttext_saved(model_path):
    model = FastText.load(model_path)
    return model


def load_fastText_bin(model_path):
    model = FastText.load_fasttext_format(model_path)
    return model

# ===============================
# PREPROCESSING
# ===============================


def create_ngrams(tokens, n=2):
    return np.array(list(ngrams(tokens, n))).tolist()


def create_ngram_list(tokens, max_n=2):
    out = []
    for n in range(1, max_n+1):
        ngram_list = create_ngrams(tokens, n)
        out += ngram_list
    return out


def create_processing_fn(nlp, stopwords):

    def preprocess(sent):
        tokens = [t for t in nlp(sent) if not t.pos_ in [
            'PUNCT', 'DET'] and len(t.lemma_.strip()) > 0]
        tokens2 = []
        for t in tokens:
            if t.pos_ in ['PRON', 'PROPN']:
                tokens2.append(t.text.lower())
            else:
                tokens2.append(t.lemma_)
        tokens3 = [t for t in tokens2 if not t in stopwords]
        return tokens3

    return preprocess

# ===============================
# SIMILARITY FUNCTIONS
# ===============================


def entailment_similarity(model_answer_ngrams, student_answer_ngrams, sim_fn, length_penalize=False):
    k = len(model_answer_ngrams)

    total_score = 0

    for model_item in model_answer_ngrams:

        max_item_score = 0

        for student_item in student_answer_ngrams:

            # never compare n-grams of different sizes
            if len(model_item) != len(student_item):
                continue

            n = len(model_item)
            item_score = sim_fn(model_item, student_item)

            if item_score < 0:
                continue

            item_score = np.power(item_score, (1/n))

            if item_score > max_item_score:
                max_item_score = item_score

        total_score += max_item_score

    if length_penalize:
        k = np.max([len(model_answer_ngrams), len(student_answer_ngrams)])

    return total_score / (k + 1e-30)


def create_phrase_similarity_function(model):
    def phrase_vector_similarity(v1, v2):
        n = len(v1)
        sim = 0.0
        try:
            if n == 1:
                sim = model.similarity(v1[0], v2[0])
            else:
                sim = model.n_similarity(v1, v2)
        except KeyError:
            # some word in the sequence is not in the vocabulary.
            pass
        return sim

    return phrase_vector_similarity


def create_word_to_word_similarity_function(model):

    def word_to_word_vector_similarity(v1, v2):
        n = len(v1)
        if n == 0:
            return 0.0

        sims = 0
        for i in range(len(v1)):
            x1 = v1[i]
            x2 = v2[i]

            try:
                sims += model.similarity(x1, x2)
            except KeyError:
                # word x1 or x2 is not in vocabulary
                pass

        return sims / n

    return word_to_word_vector_similarity


def create_compute_entailment_similarity(sim_fn, ngram_size):

    def compute_entailment_similarity(ma, sa):
        ma_ng = create_ngram_list(ma, ngram_size)
        sa_ng = create_ngram_list(sa, ngram_size)

        score = entailment_similarity(
            ma_ng, sa_ng, sim_fn=sim_fn, length_penalize=False)

        return score

    return compute_entailment_similarity


def create_vector_cosine_similarity(nlp):

    def spacy_cosine_similarity(ma, sa):
        """
        Uses Spacy's built in Cosine Similarity Score.

        1. Average Word Vectors
        2. Calculate Cosine Similarity 
        """
        return nlp(ma).similarity(nlp(sa))

    return spacy_cosine_similarity


MAX_WMD_VALUE = 3


def create_wmdistance(model):

    def word_mover_distance(s1, s2):
        """
        Uses gensims built in Word Mover Distance measurement.
        """
        value = model.wmdistance(s1, s2)
        if np.isinf(value) or np.isnan(value):
            return MAX_WMD_VALUE
        else:
            return value

    return word_mover_distance


def create_wmsimilarity(model):

    def word_mover_similarity(s1, s2):
        """
        Uses gensims built in Word Mover Distance measurement.
        """
        value = model.wmdistance(s1, s2)
        if np.isinf(value) or np.isnan(value):
            return 0.0
        else:
            return max(1.0 - (value/MAX_WMD_VALUE), 0.0)

    return word_mover_similarity
