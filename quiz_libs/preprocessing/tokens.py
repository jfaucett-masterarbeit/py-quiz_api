from nltk.corpus import stopwords
from nltk.tokenize import WhitespaceTokenizer
from nltk.stem import SnowballStemmer
import string
import numpy as np
import spacy

nlp_en = spacy.load('en')
EN_STOPS = stopwords.words('english')
EN_STEMMER = SnowballStemmer('english')


def bow_tokenize(sentence):
    tok = WhitespaceTokenizer()
    tokens = tok.tokenize(sentence)
    tokens = remove_punctuation(tokens)
    return stem(tokens)


def whitespace_tokenize(sentence):
    tok = WhitespaceTokenizer()
    return tok.tokenize(sentence)


def clean_word(word):
    return word.replace('.', '').replace('-', '').replace(':', '').replace(';', '').replace(',', '')


def remove_punctuation(tokens):
    return [clean_word(w) for w in tokens if not w in string.punctuation and len(clean_word(w)) > 0]


def lowercase(tokens):
    return [w.lower() for w in tokens]


def remove_stopwords(tokens, stopwords=EN_STOPS):
    return [w for w in tokens if not w in stopwords]


def stem(tokens, stemmer=EN_STEMMER):
    return [stemmer.stem(w) for w in tokens]


def create_pipeline(pipeline_map, tokenizer):

    def pipeline(sentence):
        tokens = tokenizer(sentence)
        result = tokens.copy()
        for step, func in pipeline_map.items():
            try:
                result = func(result)
            except Exception as err:
                print("[{}]: failed.".format(step))
                print("[reason]: {}".format(err))

        return result

    return pipeline

# ===================================
# PREPROCESSING PIPELINE FUNCTIONS
# ===================================


def create_preprocess_not(nlp):

    def preprocess_not(x):
        x = nlp(x)
        tokens = []
        prefix = ''
        for t in x:
            if t.dep_ == 'neg':
                prefix = 'neg_'
                continue
            elif t.dep_ == 'punct':
                prefix = ''
                continue

            text = ''
            if t.pos_ == 'PRON':
                text = t.text.lower()
            else:
                text = t.lemma_
            fmt = '{}{}'.format(prefix, text)
            tokens.append(fmt)
        return tokens

    return preprocess_not


def create_preprocess_basic(nlp):

    def preprocess_basic_fn(x):
        tokens = []
        for t in nlp(x):
            if t.pos_ in ['PUNCT', 'DET'] or len(t.text.lower()) < 1:
                continue
            if t.pos_ in ['PRON', 'PROPN']:
                text = t.text.lower()
            else:
                text = t.lemma_
            tokens.append(text)
        return tokens

    return preprocess_basic_fn
