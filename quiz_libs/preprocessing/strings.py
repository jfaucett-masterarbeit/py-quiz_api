import re


def lowercase(text):
    return text.lower()


def normalize_whitespace(text):
    """
    Replaces multiple whitespace characters with a single character.
    It also removes all prefixed and trailing whitespace characters completely.
    """
    return re.sub(r'(\s)+', r' ', text.strip())
