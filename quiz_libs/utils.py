import os
import i18n


AVAILABLE_LANGUAGES = ['en']


def positive_nonzero(x):
    return (x > 0, 'must be >= zero')


def not_empty(x):
    return (len(x) > 0, "cannot be empty")


def load_translations(locale):
    root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    locales_dir = os.path.join(root_dir, "locales")
    i18n.load_path.append(locales_dir)

    if locale in AVAILABLE_LANGUAGES:
        i18n.config.set('error_on_missing_translation', True)
        i18n.config.set('error_on_missing_placeholder', True)
        i18n.config.set('error_on_missing_plural', True)
        i18n.config.set('available_locales', AVAILABLE_LANGUAGES)
        i18n.config.set('filename_format', '{locale}.{format}')
        i18n.set('locale', locale)
    else:
        raise Exception("error.locale_not_available: {}".format(locale))
