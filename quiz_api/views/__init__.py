from quiz_api.views.main import IndexView
from quiz_api.views.short_answer import ShortAnswerView
from quiz_api.views.fill_in_the_blank import FillInTheBlankView
