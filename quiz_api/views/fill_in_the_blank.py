from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

import i18n
import logging
from .utils import merge_dicts
from api_params.schema import Schema, Validator
from api_params.types import Type
from quiz_libs.service import dispatch_question
from quiz_libs.utils import load_translations, not_empty, positive_nonzero

# Get an instance of a logger
logger = logging.getLogger(__name__)


def build_validator():

    s = Schema()

    s.field('slots', Type.LIST, required=True)
    s.field('slot_answers', Type.LIST, required=True)
    s.field('points_available', Type.INTEGER, required=True, coerce=True)
    s.field('options', Type.HASH)
    s.field('language', Type.STRING)
    s.field('debug', Type.BOOLEAN)

    s.field('pass_fail', Type.BOOLEAN)
    s.field('pass_fail_threshold', Type.FLOAT)

    v = Validator(s)
    v.add_validator('slots', not_empty)
    v.add_validator('slot_answers', not_empty)
    v.add_validator('points_available', positive_nonzero)
    return v


validator = build_validator()


class FillInTheBlankView(APIView):

    PARAMETER_DEFAULTS = {
        'debug': True,
        'language': 'en',
        'pass_fail': False,
        'options': {
            'edit_distance': 0,
            'normalize_case': False
        },
        'pass_fail_threshold': 0.5,
        'pass_fail': False
    }

    def get(self, request):

        parameters = {
            "slots": {"type": "list", "required": True, "description": "The student's answer."},
            "slot_answers": {"type": "list", "required": True, "description": "A list of lists, each list holds the possible correct answers which a student can enter."},
            "points_available": {"type": "integer", "required": True, "description": "The number of points available for answering the question correctly."},
            "options": {"type": "string", "required": True, "description": "One can pass 'edit_distance' with an integer value or 'normalize_case' with true value."},
            "language": {"type": "string", "required": False, "description": "The language for processing. Defaults to English. Currently, only english i.e. 'en' value is available."},
            "debug": {"type": "boolean", "required": False, "description": "Return debugging information in the response."},
            "pass_fail": {"type": "boolean", "required": False, "description": "Sets the method as only checking for pass or fail."},
            "pass_fail_threshold": {"type": "float", "required": False, "description": "Sets the threshold under which all values are considered a failing score. Only valid values are in the range of [0,1]."}
        }

        return Response({"params": parameters}, status=status.HTTP_200_OK)

    def post(self, request):

        success, parsed_params = validator.validate(request.data)

        if success:

            # setup final params
            merged_params = merge_dicts(
                FillInTheBlankView.PARAMETER_DEFAULTS, parsed_params)

            # set the question_type
            merged_params['question_type'] = 'fill_in_the_blank'

            load_translations(merged_params['language'])

            result = dispatch_question(merged_params)

            return Response({'data': result}, status=status.HTTP_200_OK)
        else:
            return Response({'data': parsed_params}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
