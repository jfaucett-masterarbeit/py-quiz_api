def is_empty_dict(d):
    return len(d) == 0


def merge_dicts(x, y):
    tmp = x.copy()
    tmp.update(y)
    return tmp
