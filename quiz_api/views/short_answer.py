from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView

import logging
from .utils import merge_dicts
from api_params.schema import Schema, Validator
from api_params.types import Type

from quiz_libs.service import dispatch_question
from quiz_libs.utils import positive_nonzero, not_empty

# Get an instance of a logger
logger = logging.getLogger(__name__)


def build_validator():

    s = Schema()
    s.field('model_answer', Type.STRING, required=True)
    s.field('student_answer', Type.STRING, required=True)
    s.field('points_available', Type.INTEGER, required=True, coerce=True)
    s.field('method', Type.STRING, required=True)
    s.field('method_options', Type.HASH)
    s.field('language', Type.STRING)
    s.field('debug', Type.BOOLEAN)

    s.field('pass_fail', Type.BOOLEAN)
    s.field('pass_fail_threshold', Type.FLOAT)

    v = Validator(s)
    v.add_validator('model_answer', not_empty)
    v.add_validator('student_answer', not_empty)
    v.add_validator('method', not_empty)
    v.add_validator('points_available', positive_nonzero)

    return v


validator = build_validator()


class ShortAnswerView(APIView):

    PARAMETER_DEFAULTS = {
        'pass_fail': False,
        'pass_fail_threshold': 0.5,
        'debug': False,
        'language': 'en',
        'method_options': {}
    }

    def get(self, request):

        parameters = {
            "model_answer": {"type": "string", "required": True, "description": "The teacher's reference answer."},
            "student_answer": {"type": "string", "required": True, "description": "The student's answer."},
            "points_available": {"type": "integer", "required": True, "description": "The number of points available for answering the question correctly."},
            "method": {"type": "string", "required": True, "description": "Which ASAG method to use. Currently, available values are [cosine_coefficient, faucett_coefficient, ml_model_v1.0]"},
            "method_options": {"type": "string", "required": True, "description": "Some methods can have options. This parameter allows for passing options to an ASAG method."},
            "domain": {"type": "string", "required": True, "description": "The subject or domain of in which the question takes place; one of [computer_science, biology]"},
            "language": {"type": "string", "required": False, "description": "The language for processing. Defaults to English. Currently, only english i.e. 'en' value is available."},
            "debug": {"type": "boolean", "required": False, "description": "Return debugging information in the response."},
            "pass_fail": {"type": "boolean", "required": False, "description": "Sets the method as only checking for pass or fail."},
            "pass_fail_threshold": {"type": "float", "required": False, "description": "Sets the threshold under which all values are considered a failing score. Only valid values are in the range of [0,1]."}
        }

        return Response({"params": parameters}, status=status.HTTP_200_OK)

    def post(self, request):

        success, parsed_params = validator.validate(request.data)

        if success:
            merged_params = merge_dicts(
                ShortAnswerView.PARAMETER_DEFAULTS, parsed_params)

            # set the question_type
            merged_params['question_type'] = 'short_answer'

            result = dispatch_question(merged_params)

            return Response({'data': result}, status=status.HTTP_200_OK)
        else:
            return Response({'data': parsed_params}, status=status.HTTP_422_UNPROCESSABLE_ENTITY)
