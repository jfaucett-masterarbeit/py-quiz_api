from django.http import JsonResponse
from django.views.generic import View


class IndexView(View):
    template_name = 'main/index.html'

    def get(self, request):
        api_data = {
            'version': '1.0',
            'endpoints': [
                {
                    'url': '/api/question_types/short_answer',
                    'methods': ['GET', 'POST'],
                    'description': 'Automatically grade short answer questions'
                },
                {
                    'url': '/api/question_types/fill_in_the_blank',
                    'methods': ['GET', 'POST'],
                    'description': 'Automatically grade fill in the blank questions'
                }
            ]
        }

        return JsonResponse(api_data)
