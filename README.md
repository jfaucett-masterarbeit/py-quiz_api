# Quiz API

The quiz API provides an implementation for some of the more complicated question types in Stembord.
Currently, it supports only _fill-in-the-blank_ and _short-answer_ question types, eventually an _essay_ grading type and more advanced mathematics question types are planned.

For a description of the architecture of this API see: [Arch](./ARCH.md)

note: the quiz api currently only runs on Linux machines due to libc dependencies.

## Installation

All the required libraries for developing the quiz API are listed in the `requirements-dev.txt` file inside the scripts folder. Run the following command to install all dependencies.

```bash
pip install -r scripts/requirements-dev.txt
```

#### NLTK

Open up a terminal and issue the following commands which downloads all the NTLK datasets.

```python
import nltk
nltk.download()
```

#### Spacy Models

If you want the backend to work you need to at least install the English (en) spacy models.

```bash
python -m spacy download en
python -m spacy download es # (optional)
python -m spacy download de # (optional)
```

#### Loading Trained Vector Space Models

The `vector_space_models` directory holds all the trained word embedding models. These are far too large
to track with git so they need to be saved into the directory and named. The process right now looks like this:

Make sure you are in the project root directory and issue the following commands.

```
import gensim.downloader as api
model_en = api.load('fasttext-wiki-news-subwords-300')
model_en.save("./vector_space_models/fasttext")
```

This will download the fasttext pretrained vectors and save them to the `vector_space_models` directory. If we don't do this it takes forever to load and process the vectors because they have to be unzipped and parsed every time. Doing this saves us those steps and makes working on and booting the application much faster.

## Running the Application

To start the application and make it accessible at http://localhost:4010, run the following command:

```bash
python manage.py runserver 4010
```

## Testing the Application

To test the application, make sure you are in the root project directory and run:

```bash
pytest --disable-warnings
```

Warnings are disabled to make the output more readable.

#### Running Examples

For experimenting with the API, there is an examples folder which has files which contain a description for the action, the URL, the http headers, and the json request body. Anyone can use these to test out the apply and see how it works (with a tool like CURL or Postman for instance).

see: `./examples/` directory.
